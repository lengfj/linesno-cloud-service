package com.alinesno.cloud.portal.desktop.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.bean.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.controller.LocalMethodBaseController;
import com.alinesno.cloud.portal.desktop.web.entity.LinkPathTypeEntity;
import com.alinesno.cloud.portal.desktop.web.service.ILinkPathTypeService;

/**
 * <p> 前端控制器 </p>
 *
 * @author LuoAnDong
 * @since 2019-05-18 14:46:04
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("portal/desktop/web/linkPathType")
public class LinkPathTypeController extends LocalMethodBaseController<LinkPathTypeEntity , ILinkPathTypeService> {
	
	//日志记录
	private static final Logger log = LoggerFactory.getLogger(LinkPathTypeController.class);

	@TranslateCode(value="[{hasStatus:has_status}]")
	@ResponseBody
	@PostMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, feign , page) ;
    }


}


























