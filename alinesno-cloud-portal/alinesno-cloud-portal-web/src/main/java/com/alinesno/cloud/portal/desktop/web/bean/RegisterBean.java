package com.alinesno.cloud.portal.desktop.web.bean;

/**
 * 注册实体对象 
 * @author LuoAnDong
 * @since 2019年7月26日 下午4:46:45
 */
public class RegisterBean {

	private String phone ; // 手机号
	private String password ; // 密码
	private String phoneCode ; // 手机验证码
	private boolean original ; // 是否同意协议
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhoneCode() {
		return phoneCode;
	}
	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}
	public boolean isOriginal() {
		return original;
	}
	public void setOriginal(boolean original) {
		this.original = original;
	}
	
}
