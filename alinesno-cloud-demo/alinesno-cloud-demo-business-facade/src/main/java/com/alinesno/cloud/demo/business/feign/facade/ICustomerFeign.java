package com.alinesno.cloud.demo.business.feign.facade;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.alinesno.cloud.demo.business.feign.dto.CustomerDto;

@FeignClient("alinesno-cloud-demo-business")
public interface ICustomerFeign {

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public  ResponseEntity<CustomerDto> findById(@RequestParam("id") String id);

	@RequestMapping(value = "/logger/findAll", method = RequestMethod.GET)
	public ResponseEntity<CustomerDto> findAll();

	@RequestMapping(value = "/logger/count", method = RequestMethod.GET)
	public ResponseEntity<Long> count();

	@RequestMapping(value = "/customer/hello/{name}")
	public ResponseEntity<String> hello(@PathVariable("name") String name);

	@PostMapping("/logger/save")
	public void save(CustomerDto dto);
}