package com.alinesno.cloud.base.boot.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import com.alinesno.cloud.common.web.enable.EnableAlinesnoCommonLogin;

/**
 * 启动入口
 * 
 * @author LuoAnDong
 * @since 2018年8月7日 上午8:45:02
 */
@EnableEurekaClient
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableAlinesnoCommonLogin
public class AlinesnoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlinesnoApplication.class, args);
	}
	
}
