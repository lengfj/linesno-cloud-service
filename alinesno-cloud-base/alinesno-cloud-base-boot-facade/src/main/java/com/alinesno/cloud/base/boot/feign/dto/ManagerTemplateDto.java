package com.alinesno.cloud.base.boot.feign.dto;

import java.util.Date;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:00:40
 */
@SuppressWarnings("serial")
public class ManagerTemplateDto extends BaseDto {

    /**
     * 模板名称
     */
	private String templateName;
	
    /**
     * 模板内容
     */
	private String templateContent;
	
    /**
     * 模板数据
     */
	private String templateData;
	
    /**
     * 模板时间
     */
	private Date templateAddtime;
	
    /**
     * 模板状态
     */
	private String templateStatus;
	
    /**
     * 所属菜单
     */
	private String resourceId;
	
    /**
     * 模板作者
     */
	private String templateOwner;
	


	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getTemplateContent() {
		return templateContent;
	}

	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}

	public String getTemplateData() {
		return templateData;
	}

	public void setTemplateData(String templateData) {
		this.templateData = templateData;
	}

	public Date getTemplateAddtime() {
		return templateAddtime;
	}

	public void setTemplateAddtime(Date templateAddtime) {
		this.templateAddtime = templateAddtime;
	}

	public String getTemplateStatus() {
		return templateStatus;
	}

	public void setTemplateStatus(String templateStatus) {
		this.templateStatus = templateStatus;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getTemplateOwner() {
		return templateOwner;
	}

	public void setTemplateOwner(String templateOwner) {
		this.templateOwner = templateOwner;
	}

}
