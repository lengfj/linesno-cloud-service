package com.alinesno.cloud.base.boot.feign.dto;

import java.util.Date;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:00:40
 */
@SuppressWarnings("serial")
public class ManagerTenantLogDto extends BaseDto {

    /**
     * 租户操作类型(0登陆/1退出/2充值/3提取)
     */
	private String actionType;
	
    /**
     * 日志内容 
     */
	private String logContent;
	
    /**
     * 结束时间
     */
	private Date endTime;
	
    /**
     * 日志渠道
     */
	private String logChannel;
	
    /**
     * 详细ip
     */
	private String logIp;
	
    /**
     * 日志机器码
     */
	private String logMachine;
	
    /**
     * 业务id
     */
	private String logBusinessId;
	


	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getLogContent() {
		return logContent;
	}

	public void setLogContent(String logContent) {
		this.logContent = logContent;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getLogChannel() {
		return logChannel;
	}

	public void setLogChannel(String logChannel) {
		this.logChannel = logChannel;
	}

	public String getLogIp() {
		return logIp;
	}

	public void setLogIp(String logIp) {
		this.logIp = logIp;
	}

	public String getLogMachine() {
		return logMachine;
	}

	public void setLogMachine(String logMachine) {
		this.logMachine = logMachine;
	}

	public String getLogBusinessId() {
		return logBusinessId;
	}

	public void setLogBusinessId(String logBusinessId) {
		this.logBusinessId = logBusinessId;
	}

}
