package com.alinesno.cloud.base.boot.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:00:40
 */
@SuppressWarnings("serial")
public class InfoSchoolDto extends BaseDto {

    /**
     * 所属者
     */
	private String owners;
	
    /**
     * 学校地址
     */
	private String schoolAddress;
	
    /**
     * 学校名称
     */
	private String schoolName;
	
    /**
     * 办学类型代码
     */
	private String schoolType;
	
    /**
     * 学校代码
     */
	private String schoolCode;
	
	private String schoolProvince;
	
	private String schoolProvinceCode;
	
    /**
     * 办学类型名称
     */
	private String schoolTypeName;
	
    /**
     * 学校性质类型
     */
	private String schoolProperties;
	
    /**
     * 办学性质类型代码
     */
	private String schoolPropertiesCode;
	
    /**
     * 学校举办者
     */
	private String schoolOwner;
	
    /**
     * 学校举办者代码
     */
	private String schoolOwnerCode;
	


	public String getOwners() {
		return owners;
	}

	public void setOwners(String owners) {
		this.owners = owners;
	}

	public String getSchoolAddress() {
		return schoolAddress;
	}

	public void setSchoolAddress(String schoolAddress) {
		this.schoolAddress = schoolAddress;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolType() {
		return schoolType;
	}

	public void setSchoolType(String schoolType) {
		this.schoolType = schoolType;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getSchoolProvince() {
		return schoolProvince;
	}

	public void setSchoolProvince(String schoolProvince) {
		this.schoolProvince = schoolProvince;
	}

	public String getSchoolProvinceCode() {
		return schoolProvinceCode;
	}

	public void setSchoolProvinceCode(String schoolProvinceCode) {
		this.schoolProvinceCode = schoolProvinceCode;
	}

	public String getSchoolTypeName() {
		return schoolTypeName;
	}

	public void setSchoolTypeName(String schoolTypeName) {
		this.schoolTypeName = schoolTypeName;
	}

	public String getSchoolProperties() {
		return schoolProperties;
	}

	public void setSchoolProperties(String schoolProperties) {
		this.schoolProperties = schoolProperties;
	}

	public String getSchoolPropertiesCode() {
		return schoolPropertiesCode;
	}

	public void setSchoolPropertiesCode(String schoolPropertiesCode) {
		this.schoolPropertiesCode = schoolPropertiesCode;
	}

	public String getSchoolOwner() {
		return schoolOwner;
	}

	public void setSchoolOwner(String schoolOwner) {
		this.schoolOwner = schoolOwner;
	}

	public String getSchoolOwnerCode() {
		return schoolOwnerCode;
	}

	public void setSchoolOwnerCode(String schoolOwnerCode) {
		this.schoolOwnerCode = schoolOwnerCode;
	}

}
