package com.alinesno.cloud.base.boot.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2019-04-17 07:44:37
 */
@SuppressWarnings("serial")
public class ManagerSourceGenerateDto extends BaseDto {

	private String applicationName ; // 应用名称
	private String authorName;
	private String dbDriver;
	private String dbPwd;
	private String dbUrl;
	private String dbUser;
	private String bootPrefix;
	private String feignServerPath;
	private String packageName;
	
	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getDbDriver() {
		return dbDriver;
	}

	public void setDbDriver(String dbDriver) {
		this.dbDriver = dbDriver;
	}

	public String getDbPwd() {
		return dbPwd;
	}

	public void setDbPwd(String dbPwd) {
		this.dbPwd = dbPwd;
	}

	public String getDbUrl() {
		return dbUrl;
	}

	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}

	public String getDbUser() {
		return dbUser;
	}

	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	}

	public String getBootPrefix() {
		return bootPrefix;
	}

	public void setBootPrefix(String bootPrefix) {
		this.bootPrefix = bootPrefix;
	}

	public String getFeignServerPath() {
		return feignServerPath;
	}

	public void setFeignServerPath(String feignServerPath) {
		this.feignServerPath = feignServerPath;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

}
