package com.alinesno.cloud.base.print.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-05-03 14:07:38
 */
@Entity
@Table(name="namespace")
public class NamespaceEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 模板空间名称
     */
	private String namespace;
    /**
     * 模板空间排序
     */
	private Integer namespaceSort;


	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public Integer getNamespaceSort() {
		return namespaceSort;
	}

	public void setNamespaceSort(Integer namespaceSort) {
		this.namespaceSort = namespaceSort;
	}


	@Override
	public String toString() {
		return "NamespaceEntity{" +
			"namespace=" + namespace +
			", namespaceSort=" + namespaceSort +
			"}";
	}
}
