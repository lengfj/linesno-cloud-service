package com.alinesno.cloud.base.print.enums;

/**
 * 模板编辑状态
 * @author LuoAnDong
 * @since 2019年6月19日 下午9:35:29
 */
public enum EditStatusEnums {

	DRAFT("0" , "草稿") , 
	NORMAL("1" , "正常") ; 
	
	private String status ; 
	private String text ; 

	EditStatusEnums(String status , String text) {
		this.status = status ; 
		this.text = text ; 
	}

	public String getStatus() {
		return status;
	}

	public String getText() {
		return text;
	}
	
}
