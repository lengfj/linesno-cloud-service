package com.alinesno.cloud.base.boot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Entity
@Table(name="manager_account_role")
public class ManagerAccountRoleEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 角色Id
     */
	@Column(name="role_id")
	private String roleId;

	/**
	 * 账户id
	 */
	@Column(name="account_id")
	private String accountId;

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
}
