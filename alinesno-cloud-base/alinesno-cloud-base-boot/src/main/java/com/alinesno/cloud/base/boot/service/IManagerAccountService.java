package com.alinesno.cloud.base.boot.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.boot.entity.ManagerAccountEntity;
import com.alinesno.cloud.base.boot.repository.ManagerAccountRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@NoRepositoryBean
public interface IManagerAccountService extends IBaseService<ManagerAccountRepository, ManagerAccountEntity, String> {
	
	/**
	 * 通过用户名查询用户信息
	 * @return
	 */
	ManagerAccountEntity findByLoginName(String loginName);

}
