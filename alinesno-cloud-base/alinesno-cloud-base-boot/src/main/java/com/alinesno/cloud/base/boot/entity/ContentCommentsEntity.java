package com.alinesno.cloud.base.boot.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Entity
@Table(name="content_comments")
public class ContentCommentsEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 评论文章
     */
	@Column(name="comment_post_id")
	private Long commentPostId;
    /**
     * 评论作者
     */
	@Column(name="comment_author")
	private String commentAuthor;
    /**
     * 评论作者邮箱
     */
	@Column(name="comment_author_email")
	private String commentAuthorEmail;
    /**
     * 评论作者链接
     */
	@Column(name="comment_author_url")
	private String commentAuthorUrl;
    /**
     * 评论作者IP
     */
	@Column(name="comment_author_ip")
	private String commentAuthorIp;
    /**
     * 评论时间 
     */
	@Column(name="comment_date")
	private Date commentDate;
    /**
     * 评论修改时间
     */
	@Column(name="comment_date_gmt")
	private Date commentDateGmt;
    /**
     * 评论内容
     */
	@Column(name="comment_content")
	private String commentContent;
    /**
     * 评论用户Id
     */
	@Column(name="user_id")
	private Long userId;


	public Long getCommentPostId() {
		return commentPostId;
	}

	public void setCommentPostId(Long commentPostId) {
		this.commentPostId = commentPostId;
	}

	public String getCommentAuthor() {
		return commentAuthor;
	}

	public void setCommentAuthor(String commentAuthor) {
		this.commentAuthor = commentAuthor;
	}

	public String getCommentAuthorEmail() {
		return commentAuthorEmail;
	}

	public void setCommentAuthorEmail(String commentAuthorEmail) {
		this.commentAuthorEmail = commentAuthorEmail;
	}

	public String getCommentAuthorUrl() {
		return commentAuthorUrl;
	}

	public void setCommentAuthorUrl(String commentAuthorUrl) {
		this.commentAuthorUrl = commentAuthorUrl;
	}

	public String getCommentAuthorIp() {
		return commentAuthorIp;
	}

	public void setCommentAuthorIp(String commentAuthorIp) {
		this.commentAuthorIp = commentAuthorIp;
	}

	public Date getCommentDate() {
		return commentDate;
	}

	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}

	public Date getCommentDateGmt() {
		return commentDateGmt;
	}

	public void setCommentDateGmt(Date commentDateGmt) {
		this.commentDateGmt = commentDateGmt;
	}

	public String getCommentContent() {
		return commentContent;
	}

	public void setCommentContent(String commentContent) {
		this.commentContent = commentContent;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}


	@Override
	public String toString() {
		return "ContentCommentsEntity{" +
			"commentPostId=" + commentPostId +
			", commentAuthor=" + commentAuthor +
			", commentAuthorEmail=" + commentAuthorEmail +
			", commentAuthorUrl=" + commentAuthorUrl +
			", commentAuthorIp=" + commentAuthorIp +
			", commentDate=" + commentDate +
			", commentDateGmt=" + commentDateGmt +
			", commentContent=" + commentContent +
			", userId=" + userId +
			"}";
	}
}
