package com.alinesno.cloud.base.notice.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;
import java.util.Date;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 07:19:08
 */
@SuppressWarnings("serial")
public class EmailTemplateDto extends BaseDto {

	private String templateContent;
	
	private String templateName;
	
	private Date templateUseTime;
	
	private Integer templateVersion;
	


	public String getTemplateContent() {
		return templateContent;
	}

	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public Date getTemplateUseTime() {
		return templateUseTime;
	}

	public void setTemplateUseTime(Date templateUseTime) {
		this.templateUseTime = templateUseTime;
	}

	public Integer getTemplateVersion() {
		return templateVersion;
	}

	public void setTemplateVersion(Integer templateVersion) {
		this.templateVersion = templateVersion;
	}

}
