package com.alinesno.cloud.base.logger.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.base.logger.entity.LogLoginEntity;
import com.alinesno.cloud.base.logger.service.ILogLoginService;
import com.alinesno.cloud.common.core.rest.BaseRestController;

/**
 * <p> 接口 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:16:06
 */
@Scope("prototype")
@RestController
@RequestMapping("logLogin")
public class LogLoginRestController extends BaseRestController<LogLoginEntity , ILogLoginService> {

	//日志记录
	@SuppressWarnings("unused")
	private final static Logger logger = LoggerFactory.getLogger(LogLoginRestController.class);

}
