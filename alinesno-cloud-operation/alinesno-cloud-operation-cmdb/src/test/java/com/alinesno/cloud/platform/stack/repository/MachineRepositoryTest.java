package com.alinesno.cloud.platform.stack.repository;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.operation.cmdb.entity.MachineEntity;
import com.alinesno.cloud.operation.cmdb.repository.MachineRepository;

/**
 * 保存商品
 * @author LuoAnDong
 * @since 2018年9月9日 下午9:36:18
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MachineRepositoryTest {

	@SuppressWarnings("unused")
	private final Logger logger = LoggerFactory.getLogger(this.getClass()) ; 

	@Autowired
	private MachineRepository machineRepository ; 

	
	@Test
	public void testSaveAllIterableOfS() {
		
		List<String> arr = new ArrayList<String>() ; 
		
		arr.add("172.20.200.24|200.35|测试机") ; 
		arr.add("172.20.200.29|200.35|测试机") ; 
		arr.add("172.20.200.31|200.1|巨石测试") ; 

		List<MachineEntity> ms = new ArrayList<MachineEntity>() ; 
		for(String s : arr) {
			System.out.println("s = " + s);
			
			String a[] = s.split("\\|") ; 
	
			MachineEntity m = new MachineEntity() ; 
			m.setMachineName(a[2]);
			m.setMachineDesc(a[2]);
			m.setMachineIp(a[0]);
			m.setMachinePwd("1234qwer");
			m.setMasterMachine("172.20."+a[1]);
			m.setMachinePic("http://data.linesno.com/20190313172617555441517385220096");
			m.setHasStatus(1);
			m.setMasterCode("001");
			
			ms.add(m) ; 
			
			System.out.println();
		}
		machineRepository.saveAll(ms) ;
	
	}

}
