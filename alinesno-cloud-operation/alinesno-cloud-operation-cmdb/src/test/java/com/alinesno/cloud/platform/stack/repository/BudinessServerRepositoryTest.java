package com.alinesno.cloud.platform.stack.repository;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.operation.cmdb.entity.ServerEntity;
import com.alinesno.cloud.operation.cmdb.repository.BudinessServerRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BudinessServerRepositoryTest {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass()) ; 

	@Autowired
	private BudinessServerRepository budinessServerRepository ; 
	
	@Test
	public void testSave() {
		ServerEntity e1 = new ServerEntity() ; 
		e1.setServerName("帮我买");
		e1.setServerCode("1");
		e1.setServerDesc("物品-外卖-药品-肯德基");
		e1.setAddTime(new Date());
		e1.setFieldProp("#e04242");
		
		ServerEntity e2 = new ServerEntity() ; 
		e2.setServerName("帮我取");
		e2.setServerCode("2");
		e2.setServerDesc("取文件-送礼品-包裹-物品-演出票");
		e2.setAddTime(new Date());;
		e2.setFieldProp("#7474e0");
		
		ServerEntity e3 = new ServerEntity() ; 
		e3.setServerName("帮我送");
		e3.setServerCode("3");
		e3.setServerDesc("取文件-送礼品-包裹-物品-演出票");
		e3.setAddTime(new Date());;
		e3.setFieldProp("#ebb030");
		
		ServerEntity e4 = new ServerEntity() ; 
		e4.setServerName("帮我办");
		e4.setServerCode("4");
		e4.setServerDesc("装系统-网站开发-冷天送衣");
		e4.setAddTime(new Date());
		e4.setFieldProp("#099c45");
		
		List<ServerEntity> es = new ArrayList<ServerEntity>() ; 
		es.add(e1) ; 
		es.add(e2) ; 
		es.add(e3) ; 
		es.add(e4) ; 
		
		Iterable<ServerEntity> b = budinessServerRepository.saveAll(es) ; 
		logger.info("b = {}" , b);
		
	}

	@Test
	public void testSaveAll() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindById() {
		fail("Not yet implemented");
	}

	@Test
	public void testExistsById() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAll() {
		Iterable<ServerEntity> list = budinessServerRepository.findAll() ; 
		logger.info("list = {}" , list);
	}

	@Test
	public void testFindAllById() {
		fail("Not yet implemented");
	}

	@Test
	public void testCount() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteById() {
		fail("Not yet implemented");
	}

	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteAllIterableOfQextendsT() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteAll() {
		fail("Not yet implemented");
	}

}
