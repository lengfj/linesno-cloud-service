package com.alinesno.cloud.operation.cmdb.common.constants;

/**
 * 订单状态
 * 
 * @author LuoAnDong
 * @since 2018年8月5日 下午6:41:23
 */
public enum RunerEnum {

	STATUS_NOT_APPLY("not_apply", "未申请"), 
	STATUS_APPLY("apply", "申请中"), 
	STATUS_UNSUB("unsub", "取消关注"), 
	STATUS_MEMBER("member", "普通会员"), 
	STATUS_RUNMAN("runman", "人员"),

	RUNER("1", "待接单"), // 人员
	NOT_RUNNER("0", "已接单"), // 申请中
	
	USER_APPLY_TYPE_SIMPLE("simple" , "简单申请")  ,
	USER_APPLY_TYPE_COMPLEX("complex" , "复杂申请")  ; 

	public String code;
	public String text;

	RunerEnum(String code, String text) {
		this.code = code;
		this.text = text;
	}

	public static RunerEnum getStatus(String code) {
		if ("1".equals(code)) {
			return RUNER;
		} else if ("2".equals(code)) {
			return NOT_RUNNER;
		} else if ("apply".equals(code)) {
			return STATUS_APPLY ; 
		} else if ("unsub".equals(code)) {
			return STATUS_UNSUB ;
		} else if ("member".equals(code)) {
			return STATUS_MEMBER ; 
		} else if ("runman".equals(code)) {
			return STATUS_RUNMAN ; 
		}
		return null;
	}

	public String getText() {
		return this.text;
	}

	public String getCode() {
		return this.code;
	}
}