package com.alinesno.cloud.operation.cmdb.repository;
import java.util.List;

import com.alinesno.cloud.operation.cmdb.entity.GoodsOrderEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface GoodsOrderRepository extends BaseJpaRepository<GoodsOrderEntity, String> {

	List<GoodsOrderEntity> findAllByGoodsOrderId(String goodsOrderId);

}