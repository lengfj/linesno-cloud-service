package com.alinesno.cloud.operation.cmdb.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alibaba.fastjson.annotation.JSONField;

import cn.afterturn.easypoi.excel.annotation.Excel;

@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_database_info")
public class DatabaseEntity extends BaseEntity {

	private String hostname ;  //主机名称
	
	@Excel(name = "主机IP")
	private String hostIp ;  //主机IP
	
	private String hostPwd ;  //主机密码
	
	@Excel(name = "登陆名")
	private String dbaLoginName ; //登陆名称
	
	@Excel(name = "数据库空间")
	private String dataSpaceName ; //数据库空间
	
	@Excel(name = "数据库空间大小")
	private String dataSpaceSize ; //数据库空间大小
	
	@Excel(name = "索引空间")
	private String indexSpaceName ; //索引空间
	
	@Excel(name = "索引空间大小")
	private String indexSpaceSize ; //索引空间大小
	
	@Excel(name = "使用描述")
	private String spaceDesc ; //数据库使用描述 
	
	@Excel(name = "管理员")
	private String managerMan ; //管理员
	
	private Integer applyStatus = 0; // 状态(0未通过|1通过)

	@Excel(name = "开始使用时间" , databaseFormat = "yyyy-MM-dd")
	@JSONField(format = "yyyy-MM-dd")
	private Date sendTime ; //开始使用时间
	
	@Excel(name = "结束使用时间" , databaseFormat = "yyyy-MM-dd")
	@JSONField(format = "yyyy-MM-dd")
	private Date endTime ; //开始使用时间
	
	public String getDataSpaceSize() {
		return dataSpaceSize;
	}
	public void setDataSpaceSize(String dataSpaceSize) {
		this.dataSpaceSize = dataSpaceSize;
	}
	public String getIndexSpaceSize() {
		return indexSpaceSize;
	}
	public void setIndexSpaceSize(String indexSpaceSize) {
		this.indexSpaceSize = indexSpaceSize;
	}
	public Integer getApplyStatus() {
		return applyStatus;
	}
	public void setApplyStatus(Integer applyStatus) {
		this.applyStatus = applyStatus;
	}
	public String getManagerMan() {
		return managerMan;
	}
	public void setManagerMan(String managerMan) {
		this.managerMan = managerMan;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public String getHostIp() {
		return hostIp;
	}
	public void setHostIp(String hostIp) {
		this.hostIp = hostIp;
	}
	public String getHostPwd() {
		return hostPwd;
	}
	public void setHostPwd(String hostPwd) {
		this.hostPwd = hostPwd;
	}
	public String getDbaLoginName() {
		return dbaLoginName;
	}
	public void setDbaLoginName(String dbaLoginName) {
		this.dbaLoginName = dbaLoginName;
	}
	public String getDataSpaceName() {
		return dataSpaceName;
	}
	public void setDataSpaceName(String dataSpaceName) {
		this.dataSpaceName = dataSpaceName;
	}
	public String getIndexSpaceName() {
		return indexSpaceName;
	}
	public void setIndexSpaceName(String indexSpaceName) {
		this.indexSpaceName = indexSpaceName;
	}
	public String getSpaceDesc() {
		return spaceDesc;
	}
	public void setSpaceDesc(String spaceDesc) {
		this.spaceDesc = spaceDesc;
	}
	public Date getSendTime() {
		return sendTime;
	}
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

}
