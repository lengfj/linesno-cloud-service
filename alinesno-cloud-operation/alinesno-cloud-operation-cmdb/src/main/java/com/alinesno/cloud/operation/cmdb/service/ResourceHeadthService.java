package com.alinesno.cloud.operation.cmdb.service;

import java.util.List;

import com.alinesno.cloud.operation.cmdb.entity.ResourceHeadthEntity;
import com.alinesno.cloud.operation.cmdb.entity.ResourceHeadthLoggerEntity;

/**
 * 健康检查服务
 * 
 * @author LuoAnDong
 * @since 2018年9月23日 上午8:30:53
 */
public interface ResourceHeadthService {

	/**
	 * 更新建议服务
	 * @param entity
	 */
	public void updateHeadth(ResourceHeadthEntity entity) ; 

	/**
	 * 保存建议服务检查记录
	 * @param entity
	 */
	public void saveHeadLogger(ResourceHeadthLoggerEntity entity) ; 

	/**
	 * 查询所有监控项
	 * @return
	 */
	public List<ResourceHeadthEntity> findAll(); 
}
