package com.alinesno.cloud.operation.cmdb.service;

import java.util.List;

import com.alinesno.cloud.operation.cmdb.entity.AccountEntity;
import com.alinesno.cloud.operation.cmdb.entity.ResourceEntity;

/**
 * 账户服务 
 * @author LuoAnDong
 * @since 2018年9月23日 上午8:30:53
 */
public interface AccountService {

	/**
	 * 通过用户id获取用户菜单 
	 * @param accountId
	 * @return
	 */
	List<ResourceEntity> findResourceByUserId(String accountId) ;

	/**
	 * 保存用户
	 * @param bean
	 * @param resources
	 * @return
	 */
	boolean saveAccount(AccountEntity bean, String resources , String remoteHost);

	/**
	 * 修改用户
	 * @param bean
	 * @param resources
	 * @param remoteHost
	 * @return
	 */
	boolean modifyAccount(AccountEntity bean, String resources, String remoteHost); 
	
}
