package com.alinesno.cloud.operation.cmdb.common.constants;

/**
 * 订单状态
 * 
 * @author LuoAnDong
 * @since 2018年8月5日 下午6:41:23
 */
public enum HasStatusEnum {

	FORBIDDEN(1 , "禁用"), // 待接单
	NORMARL(0 , "正常"); // 已接单
	
	private int code;
	private String text ;
	
	HasStatusEnum(int code , String text) {
		this.code = code;
		this.text = text ; 
	}
	
	public static HasStatusEnum getStatus(int code) {
		if(0 == code) {
			return NORMARL; 
		}else if(1 == code) {
			return HasStatusEnum.FORBIDDEN; 
		}
		return NORMARL ; 
	}
	
	public String getText() {
		return this.text ; 
	}

	public int getCode() {
		return this.code;
	}
}