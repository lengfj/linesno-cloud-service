package com.alinesno.cloud.operation.cmdb.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 通用地址 
 * 
 * @author LuoAnDong
 * @since 2018年8月5日 下午12:20:30
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_address")
public class AddressEntity extends BaseEntity {

	private String userId ; //所属用户
	private String addressName ; //地址名称
	private String floor ; //所在楼层
	private int status ; //是否使用(1使用|0未使用)
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getAddressName() {
		return addressName;
	}
	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}
	public String getFloor() {
		return floor;
	}
	public void setFloor(String floor) {
		this.floor = floor;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
