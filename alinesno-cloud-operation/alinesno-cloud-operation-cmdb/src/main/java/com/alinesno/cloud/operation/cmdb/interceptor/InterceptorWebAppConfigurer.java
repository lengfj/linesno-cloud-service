package com.alinesno.cloud.operation.cmdb.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * 拦截器配置
 * 
 * @author LuoAnDong
 * @since 2018年8月14日 上午8:16:44
 */
@Configuration
public class InterceptorWebAppConfigurer extends WebMvcConfigurationSupport {

	@Value("${wechat.dev.model}")
	private boolean wechatDev;

	@Autowired
	private ManagerSessionInterceptor managerInterceptor;

	private String[] staticPath = new String[] {"**.png","**.txt","**.ico","**.css","/static/**"} ; 

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// 多个拦截器组成一个拦截器链
		// addPathPatterns 用于添加拦截规则
		// excludePathPatterns 用户排除拦截

		registry.addInterceptor(managerInterceptor).addPathPatterns("/manager/**")
				.excludePathPatterns(new String[] { "/apply/machine" ,"/apply/database" , "/manager/login", "/manager/validate" })
				.excludePathPatterns(staticPath);

		super.addInterceptors(registry);
	}

	@Override
	protected void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
		super.addResourceHandlers(registry);
	}

}