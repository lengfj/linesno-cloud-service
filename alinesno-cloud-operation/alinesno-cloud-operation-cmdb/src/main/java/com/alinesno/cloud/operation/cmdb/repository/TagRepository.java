package com.alinesno.cloud.operation.cmdb.repository;
import com.alinesno.cloud.operation.cmdb.entity.TagEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface TagRepository extends BaseJpaRepository<TagEntity, String> {

}