package com.alinesno.cloud.operation.cmdb.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.transaction.Transactional;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.operation.cmdb.common.constants.AccountRoleEnum;
import com.alinesno.cloud.operation.cmdb.common.constants.AccountStatusEnum;
import com.alinesno.cloud.operation.cmdb.entity.AccountEntity;
import com.alinesno.cloud.operation.cmdb.entity.AccountResourceEntity;
import com.alinesno.cloud.operation.cmdb.entity.ResourceEntity;
import com.alinesno.cloud.operation.cmdb.repository.AccountRepository;
import com.alinesno.cloud.operation.cmdb.repository.AccountResourceRepository;
import com.alinesno.cloud.operation.cmdb.repository.ResourceRepository;
import com.alinesno.cloud.operation.cmdb.service.AccountService;

/**
 * 通过用户账号获取账户服务 
 * @author LuoAnDong
 * @since 2018年9月23日 上午8:31:57
 */
@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountResourceRepository accountResourceRepository ; 
	
	@Autowired
	private AccountRepository accountRepository ; 
	
	@Autowired
	private ResourceRepository resourceRepository ; 
	
	@Override
	public List<ResourceEntity> findResourceByUserId(String accountId) {
		
		AccountEntity account = accountRepository.findById(accountId).get() ; 
		
		if(account != null) {
		
			//是否为超级管理员
			if(AccountRoleEnum.ADMIN.getCode().equals(account.getRolePower())) {
				
				List<ResourceEntity> list = resourceRepository.findAll(Sort.by(Direction.DESC, "resourceSort")) ; 
				return list ; 
				
			}else {
			
				List<AccountResourceEntity> arList = accountResourceRepository.findAllByAccountId(accountId , Sort.by(Direction.DESC, "addTime")) ; 
				
				if(arList != null && arList.size() > 0) {
					List<ResourceEntity>  list = new ArrayList<ResourceEntity> () ; 
					
					for(AccountResourceEntity a : arList) {
						ResourceEntity r = resourceRepository.findById(a.getResourceId()).get() ; 
						list.add(r) ; 
					}
					
					Collections.sort(list);
					return list ; 
				}
				
			}
		}
	
		return null;
	}

	@Transactional
	@Override
	public boolean saveAccount(AccountEntity bean, String resources , String remoteHost) {
		
		String salt = String.valueOf(new Random().nextInt(999999)) ; 
		
		bean.setAccountStatus(AccountStatusEnum.NORMARL.getCode());
		bean.setSalt(salt) ; 
		bean.setLastLoginIp(remoteHost);
		bean.setPassword(DigestUtils.md5Hex(bean.getPassword()+salt));
		bean.setAddTime(new Date());

		accountRepository.save(bean) ; 
		saveAccountResource(resources, bean.getId()); 
		
		return true ;
	}
	
	private void saveAccountResource(String resources , String accountId) {
		//保存权限
		if(StringUtils.isNoneBlank(resources)) {
		
			// 删除之前的权限
			accountResourceRepository.deleteByAccountId(accountId) ; 
		
			//添加新权限 
			String[] resourceArr = resources.split(",") ; 
			List<AccountResourceEntity> resourceList = new ArrayList<AccountResourceEntity>() ; 
			
			for(String resource : resourceArr) {
				AccountResourceEntity accountResource = new AccountResourceEntity() ; 
				accountResource.setAddTime(new Date());
				accountResource.setAccountId(accountId);
				accountResource.setResourceId(resource);
				resourceList.add(accountResource) ; 
			}
			
			accountResourceRepository.saveAll(resourceList) ; 
		}
	}

	@Transactional
	@Override
	public boolean modifyAccount(AccountEntity bean , String resources, String remoteHost) {
		
		// 查看是否已经存在登陆名
		AccountEntity account = accountRepository.findByLoginName(bean.getLoginName()) ; 
		
		account.setName(bean.getName());
		account.setLoginName(bean.getLoginName());
		
		if(StringUtils.isNoneBlank(bean.getPassword())) {
			account.setPassword(DigestUtils.md5Hex(bean.getPassword()+account.getSalt()));
		}
		
		account.setUpdateTime(new Date());
		accountRepository.save(account) ; 
		
		saveAccountResource(resources, bean.getId()); 
		
		return true ;
	}

}
