package com.alinesno.cloud.operation.cmdb.repository;
import com.alinesno.cloud.operation.cmdb.entity.ResourceHeadthEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface ResourceHeadthRepository extends BaseJpaRepository<ResourceHeadthEntity, String> {
	
}