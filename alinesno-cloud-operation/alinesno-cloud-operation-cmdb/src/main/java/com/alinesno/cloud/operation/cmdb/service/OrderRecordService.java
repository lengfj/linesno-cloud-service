package com.alinesno.cloud.operation.cmdb.service;

/**
 * 订单记录服务 
 * @author LuoAnDong
 * @since 2018年8月24日 上午7:35:19
 */
public interface OrderRecordService {

	/**
	 * 订单操作记录
	 * @param orderId
	 * @param recordCode
	 * @param actionMan
	 */
	public void saveRecord(String orderId, String recordCode , String userId) ;
	
	/**
	 * 订单操作记录
	 * @param orderId
	 * @param recordCode
	 * @param actionMan
	 * @param detail 
	 */
	public void saveRecord(String orderId, String recordCode , String userId , String detail) ;
	
}
