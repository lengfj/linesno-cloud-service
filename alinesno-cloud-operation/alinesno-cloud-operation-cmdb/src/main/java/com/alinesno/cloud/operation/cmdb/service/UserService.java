package com.alinesno.cloud.operation.cmdb.service;

import java.util.List;

import com.alinesno.cloud.operation.cmdb.common.constants.ScoreActionEnum;
import com.alinesno.cloud.operation.cmdb.entity.UserEntity;

/**
 * 用户服务 
 * @author LuoAnDong
 * @since 2018年10月8日 下午10:29:09
 */
public interface UserService {

	/**
	 * 查询所有用户不包括
	 * @param memberId 会员
	 * @return
	 */
	List<UserEntity> findAllMember(String masterCode , String memberId);

	/**
	 * 用户积分添加
	 * @param userId
	 * @param score
	 * @param action
	 * @param orderId
	 * @param remark
	 * @return
	 */
	boolean recordIntegral(String userId , int score , ScoreActionEnum action , String orderId , String remark) ;

	/**
	 * 判断用户是否被禁用
	 * @param id
	 * @return true禁用|false未被禁用
	 */
	boolean isForbidden(String id); 
	
}
