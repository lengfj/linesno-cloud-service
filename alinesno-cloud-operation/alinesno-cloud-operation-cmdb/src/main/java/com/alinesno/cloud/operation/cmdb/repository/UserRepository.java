package com.alinesno.cloud.operation.cmdb.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import com.alinesno.cloud.operation.cmdb.entity.UserEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface UserRepository extends BaseJpaRepository<UserEntity, String> {

	List<UserEntity> findAllByMasterCodeAndFieldProp(String masterCode, String fieldProp);
 
	Page<UserEntity> findByFieldProp(String fieldProp, Pageable pageable);

	UserEntity findByOpenId(String openId);

	UserEntity findByPhone(String phone);
 
	List<UserEntity> findAllByFieldPropAndIdNot(String fieldProp, String id);

	List<UserEntity> findAllByMasterCodeAndFieldPropAndIdNotOrderByIntegral(String masterCode, String fieldProp, String id);

	List<UserEntity> findAllByMasterCodeAndFieldPropOrderByIntegral(String masterCode, String fieldProp);

	List<UserEntity> findAllByMasterCodeAndFieldPropAndIdNotOrderByIntegralDesc(String masterCode, String code,
			String id);

	List<UserEntity> findAllByMasterCodeAndFieldPropOrderByIntegralDesc(String masterCode, String code);

	Page<UserEntity> findAllByMasterCode(String masterCode, Specification<UserEntity> p, Pageable of);

}