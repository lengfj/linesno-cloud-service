/*
 Navicat Premium Data Transfer

 Source Server         : localhost_root
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost
 Source Database       : alinesno_cloud_platform_stack

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : utf-8

 Date: 03/06/2019 08:01:47 AM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `account`
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `account_status` varchar(255) DEFAULT NULL,
  `last_login_ip` varchar(255) DEFAULT NULL,
  `last_login_time` varchar(255) DEFAULT NULL,
  `login_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role_id` varchar(255) DEFAULT NULL,
  `role_power` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_s00mhdt1w0b4bc900jk5is3wp` (`login_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `account`
-- ----------------------------
BEGIN;
INSERT INTO `account` VALUES ('551480428817547264', '2019-03-02 19:06:20', '0', null, null, null, '0', '0', null, '0', '2019-03-06 07:57:07', '1', '0:0:0:0:0:0:0:1', '2019-03-06 07:57:06', 'admin', '超级管理员', 'admin', null, '9', '1234', null, null), ('551488609866219520', '2019-03-02 19:38:51', '0', null, null, null, '0', '0', '001', '0', '2019-03-05 20:07:26', '1', '0:0:0:0:0:0:0:1', '2019-03-05 20:07:25', '001', '广州海珠区服务器管理员', '1c7c86dd4a3524215a674fb5d3c92797', null, '0', '129678', null, null), ('552606042869989376', '2019-03-05 21:39:08', '0', null, null, null, '0', '0', null, '0', '2019-03-06 07:57:25', '1', '0:0:0:0:0:0:0:1', '2019-03-06 07:57:25', 'switch', '南宁珠海机房管理员', 'fd27d63f639b550b1af94fc440362470', null, '0', '717213', null, '002');
COMMIT;

-- ----------------------------
--  Table structure for `account_resource`
-- ----------------------------
DROP TABLE IF EXISTS `account_resource`;
CREATE TABLE `account_resource` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `account_id` varchar(255) DEFAULT NULL,
  `data_scope` varchar(255) DEFAULT NULL,
  `resource_id` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `account_resource`
-- ----------------------------
BEGIN;
INSERT INTO `account_resource` VALUES ('551488609899773952', '2019-03-02 19:38:51', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:38:51', '551488609866219520', null, '551482687840321536', null), ('551488609903968256', '2019-03-02 19:38:51', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:38:51', '551488609866219520', null, '551482687907430400', null), ('551488609903968257', '2019-03-02 19:38:51', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:38:51', '551488609866219520', null, '551482687915819008', null), ('551488609908162560', '2019-03-02 19:38:51', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:38:51', '551488609866219520', null, '551482687924207616', null), ('551488609908162561', '2019-03-02 19:38:51', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:38:51', '551488609866219520', null, '551482687928401920', null), ('552748363053793281', '2019-03-06 07:04:39', '0', null, null, null, '0', '0', null, '0', '2019-03-06 07:04:39', '552606042869989376', null, '551482687928401920', null), ('552748363053793280', '2019-03-06 07:04:39', '0', null, null, null, '0', '0', null, '0', '2019-03-06 07:04:39', '552606042869989376', null, '551482687924207616', null), ('552748363049598977', '2019-03-06 07:04:39', '0', null, null, null, '0', '0', null, '0', '2019-03-06 07:04:39', '552606042869989376', null, '551482687915819008', null), ('552748363049598976', '2019-03-06 07:04:39', '0', null, null, null, '0', '0', null, '0', '2019-03-06 07:04:39', '552606042869989376', null, '551482687915819006', null), ('552748363045404673', '2019-03-06 07:04:39', '0', null, null, null, '0', '0', null, '0', '2019-03-06 07:04:39', '552606042869989376', null, '551482687915819007', null), ('552748363045404672', '2019-03-06 07:04:39', '0', null, null, null, '0', '0', null, '0', '2019-03-06 07:04:39', '552606042869989376', null, '551482687907430400', null), ('552748363041210368', '2019-03-06 07:04:39', '0', null, null, null, '0', '0', null, '0', '2019-03-06 07:04:39', '552606042869989376', null, '551482687840321536', null), ('552748363057987584', '2019-03-06 07:04:39', '0', null, null, null, '0', '0', null, '0', '2019-03-06 07:04:39', '552606042869989376', null, '551482687920013312', null), ('552748363057987585', '2019-03-06 07:04:39', '0', null, null, null, '0', '0', null, '0', '2019-03-06 07:04:39', '552606042869989376', null, '551482687936790528', null);
COMMIT;

-- ----------------------------
--  Table structure for `address`
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `address_name` varchar(255) DEFAULT NULL,
  `floor` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `apply`
-- ----------------------------
DROP TABLE IF EXISTS `apply`;
CREATE TABLE `apply` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `apply_content` varchar(255) DEFAULT NULL,
  `apply_manager` varchar(255) DEFAULT NULL,
  `apply_status` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `deploy_info`
-- ----------------------------
DROP TABLE IF EXISTS `deploy_info`;
CREATE TABLE `deploy_info` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `business_type` varchar(255) DEFAULT NULL,
  `discount` int(11) NOT NULL,
  `end_time` datetime DEFAULT NULL,
  `floor` varchar(255) DEFAULT NULL,
  `get_address` varchar(255) DEFAULT NULL,
  `goods_name` varchar(255) DEFAULT NULL,
  `goods_order_id` varchar(255) DEFAULT NULL,
  `goods_type` varchar(255) DEFAULT NULL,
  `goods_weight` varchar(255) DEFAULT NULL,
  `machine_id` varchar(255) DEFAULT NULL,
  `order_cpu` varchar(255) DEFAULT NULL,
  `order_memory` varchar(255) DEFAULT NULL,
  `order_network` varchar(255) DEFAULT NULL,
  `order_pwd` varchar(255) DEFAULT NULL,
  `order_storage` varchar(255) DEFAULT NULL,
  `order_system` varchar(255) DEFAULT NULL,
  `order_username` varchar(255) DEFAULT NULL,
  `price_cert` varchar(255) DEFAULT NULL,
  `receive_address` varchar(255) DEFAULT NULL,
  `receive_phone` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `send_time` datetime DEFAULT NULL,
  `total_pay` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `machine_ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `deploy_info`
-- ----------------------------
BEGIN;
INSERT INTO `deploy_info` VALUES ('552744896658669568', '2019-03-06 06:50:53', '0', null, null, null, '0', '0', '002', '0', '2019-03-06 06:50:53', null, '0', '2019-03-22 00:00:00', null, null, '百色Dubbo服务器', null, null, null, '552614238661640192', '4', '12', '内外网', null, '200', 'Centos7_x64', null, null, '研发部', null, null, '2019-03-13 00:00:00', null, null, '172.20.200.124');
COMMIT;

-- ----------------------------
--  Table structure for `email`
-- ----------------------------
DROP TABLE IF EXISTS `email`;
CREATE TABLE `email` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `email_owner` varchar(255) DEFAULT NULL,
  `fail_desc` longtext,
  `fail_time` int(11) NOT NULL,
  `last_send_fila_time` datetime DEFAULT NULL,
  `rend_status` int(11) NOT NULL,
  `send_time` int(11) NOT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `success_time` int(11) NOT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `goods`
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `discount` int(11) NOT NULL,
  `goods_desc` varchar(255) DEFAULT NULL,
  `goods_name` varchar(255) DEFAULT NULL,
  `goods_num` int(11) NOT NULL,
  `goods_pic` varchar(255) DEFAULT NULL,
  `goods_prices` varchar(255) DEFAULT NULL,
  `goods_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `goods_order`
-- ----------------------------
DROP TABLE IF EXISTS `goods_order`;
CREATE TABLE `goods_order` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `discount` int(11) NOT NULL,
  `goods_desc` varchar(255) DEFAULT NULL,
  `goods_name` varchar(255) DEFAULT NULL,
  `goods_num` int(11) NOT NULL,
  `goods_order_id` varchar(255) DEFAULT NULL,
  `goods_pic` varchar(255) DEFAULT NULL,
  `goods_prices` varchar(255) DEFAULT NULL,
  `goods_status` varchar(255) DEFAULT NULL,
  `select_goods_num` int(11) NOT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `integral_record`
-- ----------------------------
DROP TABLE IF EXISTS `integral_record`;
CREATE TABLE `integral_record` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `action_code` varchar(255) DEFAULT NULL,
  `action_name` varchar(255) DEFAULT NULL,
  `action_remark` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `score_total` int(11) NOT NULL,
  `score_value` int(11) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `links`
-- ----------------------------
DROP TABLE IF EXISTS `links`;
CREATE TABLE `links` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `link_desc` varchar(255) DEFAULT NULL,
  `link_icons` varchar(255) DEFAULT NULL,
  `link_path` varchar(255) DEFAULT NULL,
  `link_tags` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `links`
-- ----------------------------
BEGIN;
INSERT INTO `links` VALUES ('552757982056677376', '2019-03-06 07:42:53', '0', null, null, null, '0', '0', '002', '0', '2019-03-06 07:42:53', '23123', null, '123123', '1'), ('552758098897403904', '2019-03-06 07:43:21', '0', null, null, null, '0', '0', '002', '0', '2019-03-06 07:43:21', '123123', null, '123123', '7'), ('552758526393450496', '2019-03-06 07:45:02', '0', null, null, null, '0', '0', '002', '0', '2019-03-06 07:45:02', '123123', null, '123123', '8'), ('552758651173994496', '2019-03-06 07:45:32', '0', null, null, null, '0', '0', '002', '0', '2019-03-06 07:45:32', '123123', null, '123123', '1');
COMMIT;

-- ----------------------------
--  Table structure for `logger`
-- ----------------------------
DROP TABLE IF EXISTS `logger`;
CREATE TABLE `logger` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `record_channel` varchar(255) DEFAULT NULL,
  `record_msg` varchar(255) DEFAULT NULL,
  `record_params` varchar(255) DEFAULT NULL,
  `record_type` varchar(255) DEFAULT NULL,
  `record_user` varchar(255) DEFAULT NULL,
  `record_user_name` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `machine`
-- ----------------------------
DROP TABLE IF EXISTS `machine`;
CREATE TABLE `machine` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `discount` int(11) NOT NULL,
  `machine_desc` varchar(255) DEFAULT NULL,
  `machine_ip` varchar(255) DEFAULT NULL,
  `machine_name` varchar(255) DEFAULT NULL,
  `machine_num` int(11) NOT NULL,
  `machine_open_port` varchar(255) DEFAULT NULL,
  `machine_pic` varchar(255) DEFAULT NULL,
  `machine_prices` varchar(255) DEFAULT NULL,
  `machine_pwd` varchar(255) DEFAULT NULL,
  `machine_rootname` varchar(255) DEFAULT NULL,
  `machine_status` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `master_machine` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `machine`
-- ----------------------------
BEGIN;
INSERT INTO `machine` VALUES ('552581798836568064', '2019-03-05 20:02:47', '0', null, null, null, '0', '0', '001', '0', '2019-03-05 20:08:06', '0', '消息中间件用品及相关管理平台', '172.20.168.111', '消息中间件', '0', null, 'http://data.linesno.com/20190305200247552581797808963584', null, 'admin', null, null, null, null, null), ('552579484394455040', '2019-03-05 19:53:36', '0', null, null, null, '0', '0', '001', '0', '2019-03-05 19:56:20', '0', '用于安装zk及相关服务，如Dubbo控制台等', '172.20.200.11', 'Zookeeper服务器', '1', null, 'http://data.linesno.com/20190305195335552579483392016384', '11', 'admin', null, '0', null, null, null), ('552580446928502784', '2019-03-05 19:57:25', '0', null, null, null, '0', '0', '001', '0', '2019-03-05 19:57:25', '0', '用于持续集成平台及管理', '172.20.200.123', '持续集成服务器', '0', null, 'http://data.linesno.com/20190305195724552580445926064128', null, 'admin', null, '0', null, null, null), ('552610167326244864', '2019-03-05 21:55:31', '0', null, null, null, '0', '0', null, '0', '2019-03-05 22:18:12', '0', '部署消息中间件及相关管理后台服务', '172.20.200.11', '消息中间件', '0', null, 'http://data.linesno.com/20190305215530552610166059565056', null, 'admin', null, '0', null, '192.168.1.123', '002'), ('552614238661640192', '2019-03-05 22:11:42', '0', null, null, null, '0', '0', null, '0', '2019-03-06 06:17:05', '0', '用于安装zk及相关服务，如Dubbo控制台等', '172.20.200.124', '持续集成服务器', '0', null, 'http://data.linesno.com/20190305221141552614237164273664', null, 'admin', null, '0', null, '172.20.168.90', '002'), ('552614472557002752', '2019-03-05 22:12:37', '0', null, null, null, '0', '0', null, '0', '2019-03-06 06:54:14', '0', '部署消息中间件及相关管理后台服务', '172.20.200.113', 'Zookeeper服务器', '0', null, 'http://data.linesno.com/20190305221237552614471529398272', null, 'admin', null, '0', null, '172.20.174.12', '002');
COMMIT;

-- ----------------------------
--  Table structure for `master_machine`
-- ----------------------------
DROP TABLE IF EXISTS `master_machine`;
CREATE TABLE `master_machine` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `master_address` varchar(255) DEFAULT NULL,
  `master_name` varchar(255) DEFAULT NULL,
  `master_order` int(11) NOT NULL,
  `master_owner` varchar(255) DEFAULT NULL,
  `master_owner_code` varchar(255) DEFAULT NULL,
  `master_properties` varchar(255) DEFAULT NULL,
  `master_properties_code` varchar(255) DEFAULT NULL,
  `master_province` varchar(255) DEFAULT NULL,
  `master_province_code` varchar(255) DEFAULT NULL,
  `master_type` varchar(255) DEFAULT NULL,
  `master_type_name` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `master_machine`
-- ----------------------------
BEGIN;
INSERT INTO `master_machine` VALUES ('552591122338152448', '2019-03-05 20:39:50', '0', null, null, null, '0', '0', null, '0', '2019-03-05 20:39:50', null, '广州海珠机房', '1', null, null, null, null, null, null, null, null, '001'), ('552605227866390528', '2019-03-05 21:35:53', '0', null, null, null, '0', '0', null, '0', '2019-03-05 21:35:53', null, '南宁珠海机房', '2', null, null, null, null, null, null, null, null, '002');
COMMIT;

-- ----------------------------
--  Table structure for `order_history`
-- ----------------------------
DROP TABLE IF EXISTS `order_history`;
CREATE TABLE `order_history` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `error_msg` longtext,
  `info_id` varchar(255) DEFAULT NULL,
  `order_evaluation` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `receive_manager_id` varchar(255) DEFAULT NULL,
  `sala_id` varchar(255) DEFAULT NULL,
  `send_status` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `order_info`
-- ----------------------------
DROP TABLE IF EXISTS `order_info`;
CREATE TABLE `order_info` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `business_type` varchar(255) DEFAULT NULL,
  `discount` int(11) NOT NULL,
  `floor` varchar(255) DEFAULT NULL,
  `get_address` varchar(255) DEFAULT NULL,
  `goods_name` varchar(255) DEFAULT NULL,
  `goods_order_id` varchar(255) DEFAULT NULL,
  `goods_type` varchar(255) DEFAULT NULL,
  `goods_weight` varchar(255) DEFAULT NULL,
  `price_cert` varchar(255) DEFAULT NULL,
  `receive_address` varchar(255) DEFAULT NULL,
  `receive_phone` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `send_time` datetime DEFAULT NULL,
  `total_pay` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  `order_cpu` varchar(255) DEFAULT NULL,
  `order_memory` varchar(255) DEFAULT NULL,
  `order_network` varchar(255) DEFAULT NULL,
  `order_pwd` varchar(255) DEFAULT NULL,
  `order_storage` varchar(255) DEFAULT NULL,
  `order_system` varchar(255) DEFAULT NULL,
  `order_username` varchar(255) DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `order_info`
-- ----------------------------
BEGIN;
INSERT INTO `order_info` VALUES ('552744638738333696', '2019-03-06 06:49:51', '0', null, null, null, '0', '0', null, '0', '2019-03-06 06:49:51', null, '0', null, null, '南宁业务服务器', null, null, null, null, '研发部', null, null, '2019-03-28 00:00:00', null, null, null, '2', '32G', '内外网', null, '200', 'Centos7_x64', null, '2019-03-27 00:00:00');
COMMIT;

-- ----------------------------
--  Table structure for `order_record`
-- ----------------------------
DROP TABLE IF EXISTS `order_record`;
CREATE TABLE `order_record` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `action_code` varchar(255) DEFAULT NULL,
  `action_detail` varchar(255) DEFAULT NULL,
  `action_man` varchar(255) DEFAULT NULL,
  `action_name` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `orders`
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `error_msg` longtext,
  `info_id` varchar(255) DEFAULT NULL,
  `order_evaluation` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `receive_manager_id` varchar(255) DEFAULT NULL,
  `sala_id` varchar(255) DEFAULT NULL,
  `send_status` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `params`
-- ----------------------------
DROP TABLE IF EXISTS `params`;
CREATE TABLE `params` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `end_time` varchar(255) DEFAULT NULL,
  `param_desc` varchar(255) DEFAULT NULL,
  `param_name` varchar(255) DEFAULT NULL,
  `param_type` varchar(255) DEFAULT NULL,
  `param_type_name` varchar(255) DEFAULT NULL,
  `param_value` varchar(255) DEFAULT NULL,
  `start_time` varchar(255) DEFAULT NULL,
  `use_param` int(11) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `params`
-- ----------------------------
BEGIN;
INSERT INTO `params` VALUES ('552591122417844224', '2019-03-05 20:39:50', '0', null, null, null, '0', '0', null, '0', '2019-03-05 20:39:50', null, '运营时间段', 'running_time', null, null, '06:12-23:59', null, '0', '001'), ('552591122422038528', '2019-03-05 20:39:50', '0', null, null, null, '0', '0', null, '0', '2019-03-05 20:39:50', null, '订单超时时间', 'order_outtime', null, null, '900', null, '0', '001'), ('552591122426232832', '2019-03-05 20:39:50', '0', null, null, null, '0', '0', null, '0', '2019-03-05 20:39:50', null, '人员订单超时时间', 'order_sala_outtime', null, null, '60', null, '0', '001'), ('552591122430427136', '2019-03-05 20:39:50', '0', null, null, null, '0', '0', null, '0', '2019-03-05 20:39:50', null, '订单最小费用', 'order_min_pay', null, null, '2', null, '0', '001'), ('552591122430427137', '2019-03-05 20:39:50', '0', null, null, null, '0', '0', null, '0', '2019-03-05 20:39:50', null, '超市时间段', 'running_shop_time', null, null, '06:12-23:59', null, '0', '001'), ('552591122434621440', '2019-03-05 20:39:50', '0', null, null, null, '0', '0', null, '0', '2019-03-05 20:39:50', null, '超市下单最小费用', 'running_shop_min_pay', null, null, '10', null, '0', '001'), ('552591122438815744', '2019-03-05 20:39:50', '0', null, null, null, '0', '0', null, '0', '2019-03-05 20:39:50', null, '用户申请类型(simple/complex)', 'user_apply_type', null, null, 'simple', null, '0', '001'), ('552591122438815745', '2019-03-05 20:39:50', '0', null, null, null, '0', '0', null, '0', '2019-03-05 20:39:50', null, '用户申请说明', 'user_apply_desc', null, null, '为保障云主机服务和客户体验,请镖师遵守如下规则:\n1. 镖师接单必须积极接单,订单任务及时处理并签收;\n2. 如因其它原因不能及时跑单,及时与客户沟通或给管理员反馈;\n3. 订单完成,镖师需在公众号及时【签收】', null, '0', '001'), ('552605227962859520', '2019-03-05 21:35:53', '0', null, null, null, '0', '0', null, '0', '2019-03-05 21:35:53', null, '运营时间段', 'running_time', null, null, '06:12-23:59', null, '0', '002'), ('552605227967053824', '2019-03-05 21:35:53', '0', null, null, null, '0', '0', null, '0', '2019-03-05 21:35:53', null, '订单超时时间', 'order_outtime', null, null, '900', null, '0', '002'), ('552605227971248128', '2019-03-05 21:35:53', '0', null, null, null, '0', '0', null, '0', '2019-03-05 21:35:53', null, '人员订单超时时间', 'order_sala_outtime', null, null, '60', null, '0', '002'), ('552605227971248129', '2019-03-05 21:35:53', '0', null, null, null, '0', '0', null, '0', '2019-03-05 21:35:53', null, '订单最小费用', 'order_min_pay', null, null, '2', null, '0', '002'), ('552605227975442432', '2019-03-05 21:35:53', '0', null, null, null, '0', '0', null, '0', '2019-03-05 21:35:53', null, '超市时间段', 'running_shop_time', null, null, '06:12-23:59', null, '0', '002'), ('552605227979636736', '2019-03-05 21:35:53', '0', null, null, null, '0', '0', null, '0', '2019-03-05 21:35:53', null, '超市下单最小费用', 'running_shop_min_pay', null, null, '10', null, '0', '002'), ('552605227983831040', '2019-03-05 21:35:53', '0', null, null, null, '0', '0', null, '0', '2019-03-05 21:35:53', null, '用户申请类型(simple/complex)', 'user_apply_type', null, null, 'simple', null, '0', '002'), ('552605227988025344', '2019-03-05 21:35:53', '0', null, null, null, '0', '0', null, '0', '2019-03-05 21:35:53', null, '用户申请说明', 'user_apply_desc', null, null, '为保障云主机服务和客户体验,请镖师遵守如下规则:\n1. 镖师接单必须积极接单,订单任务及时处理并签收;\n2. 如因其它原因不能及时跑单,及时与客户沟通或给管理员反馈;\n3. 订单完成,镖师需在公众号及时【签收】', null, '0', '002');
COMMIT;

-- ----------------------------
--  Table structure for `resources`
-- ----------------------------
DROP TABLE IF EXISTS `resources`;
CREATE TABLE `resources` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `icons` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `resource_sort` int(11) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `resources`
-- ----------------------------
BEGIN;
INSERT INTO `resources` VALUES ('551482687840321536', '2019-03-02 19:15:19', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:15:19', 'fa-tachometer-alt', '/manager/main', '仪盘表', '10', null), ('551482687907430400', '2019-03-02 19:15:19', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:15:19', 'fa-chart-area', '/manager/order_list', '申请管理', '8', null), ('551482687915819008', '2019-03-02 19:15:19', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:15:19', 'fa-tags', '/manager/machine_list', '服务管理', '6', null), ('551482687920013312', '2019-03-02 19:15:19', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:15:19', 'fa-user', '/manager/member_list', '会员管理', '1', null), ('551482687924207616', '2019-03-02 19:15:19', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:15:19', 'fa-edit', '/manager/server_list', '主机管理', '5', null), ('551482687928401920', '2019-03-02 19:15:19', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:15:19', 'fa-bell-slash', '/manager/params_list', '参数管理', '3', null), ('551482687936790528', '2019-03-02 19:15:19', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:15:19', 'fa-table', '/manager/account_list', '账户管理', '1', null), ('551482687915819007', '2019-03-02 19:15:19', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:15:19', 'fa-cubes', '/manager/deploy_list', '部署管理', '7', null), ('551482687915819006', '2019-03-02 19:15:19', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:15:19', 'fa-link', '/manager/links_list', '链接管理', '6', null);
COMMIT;

-- ----------------------------
--  Table structure for `school`
-- ----------------------------
DROP TABLE IF EXISTS `school`;
CREATE TABLE `school` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `school_address` varchar(255) DEFAULT NULL,
  `school_name` varchar(255) DEFAULT NULL,
  `school_order` int(11) NOT NULL,
  `school_owner` varchar(255) DEFAULT NULL,
  `school_owner_code` varchar(255) DEFAULT NULL,
  `school_properties` varchar(255) DEFAULT NULL,
  `school_properties_code` varchar(255) DEFAULT NULL,
  `school_province` varchar(255) DEFAULT NULL,
  `school_province_code` varchar(255) DEFAULT NULL,
  `school_type` varchar(255) DEFAULT NULL,
  `school_type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `school`
-- ----------------------------
BEGIN;
INSERT INTO `school` VALUES ('551484383555485696', null, '0', null, null, null, '0', '0', '001', '0', '2019-03-02 19:22:18', null, '广州海珠客村机房', '1', null, null, null, null, null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `score`
-- ----------------------------
DROP TABLE IF EXISTS `score`;
CREATE TABLE `score` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `score` varchar(255) DEFAULT NULL,
  `score_content` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `server`
-- ----------------------------
DROP TABLE IF EXISTS `server`;
CREATE TABLE `server` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `server_code` varchar(255) DEFAULT NULL,
  `server_desc` varchar(255) DEFAULT NULL,
  `server_name` varchar(255) DEFAULT NULL,
  `server_order` int(11) NOT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `server`
-- ----------------------------
BEGIN;
INSERT INTO `server` VALUES ('551484383589040128', '2019-03-02 19:36:11', '0', null, null, '生活', '0', '0', '001', '0', '2019-03-02 19:49:51', '1', '搭建持续集成平台', '172.20.200.9', '4', null), ('551484383593234432', '2019-03-02 19:35:33', '0', null, null, '物理机', '0', '0', '001', '0', '2019-03-02 19:35:33', '2', '测试服务器', '172.20.200.99', '3', null), ('551484383597428736', '2019-03-02 19:35:22', '0', null, null, '物理机', '0', '0', '001', '0', '2019-03-02 19:35:22', '3', '数据库服务器', '172.20.200.120', '2', null), ('551484383601623040', '2019-03-02 19:36:02', '0', null, null, '物理机', '0', '0', '001', '0', '2019-03-02 19:36:02', '4', '测试服务器', '172.20.200.87', '1', null), ('552587028735197184', '2019-03-05 20:23:34', '0', null, null, 'newBuy.png', '0', '0', null, '0', '2019-03-05 20:23:34', '1', '生活用品-水果-宵夜-早晚餐-药品等', '帮我买', '4', null), ('552587028735197185', '2019-03-05 20:23:34', '0', null, null, 'newGet.png', '0', '0', null, '0', '2019-03-05 20:23:34', '2', '快递-文件-礼物-货物等', '帮我取', '3', null), ('552587028739391488', '2019-03-05 20:23:34', '0', null, null, 'newSend.png', '0', '0', null, '0', '2019-03-05 20:23:34', '3', '打印-缴费-装系统-送东西-接待等', '帮我做', '2', null), ('552587028743585792', '2019-03-05 20:23:34', '0', null, null, 'newLine.png', '0', '0', null, '0', '2019-03-05 20:23:34', '4', '海报设计-网站开发-活动策划-简历指导', '帮设计', '1', null), ('552591122380095488', '2019-03-05 20:39:50', '0', null, null, 'newBuy.png', '0', '0', null, '0', '2019-03-05 20:39:50', '1', '生活用品-水果-宵夜-早晚餐-药品等', '帮我买', '4', '001'), ('552591122380095489', '2019-03-05 20:39:50', '0', null, null, 'newGet.png', '0', '0', null, '0', '2019-03-05 20:39:50', '2', '快递-文件-礼物-货物等', '帮我取', '3', '001'), ('552591122384289792', '2019-03-05 20:39:50', '0', null, null, 'newSend.png', '0', '0', null, '0', '2019-03-05 20:39:50', '3', '打印-缴费-装系统-送东西-接待等', '帮我做', '2', '001'), ('552591122388484096', '2019-03-05 20:39:50', '0', null, null, 'newLine.png', '0', '0', null, '0', '2019-03-05 20:39:50', '4', '海报设计-网站开发-活动策划-简历指导', '帮设计', '1', '001'), ('552605227920916480', '2019-03-05 21:37:50', '0', null, null, '使用中', '0', '0', null, '0', '2019-03-06 06:58:46', '1', '持续集成服务器', '172.20.168.90', '4', '002'), ('552605227925110784', '2019-03-05 21:35:53', '0', null, null, '使用中', '0', '0', null, '0', '2019-03-05 22:26:44', '2', '业务服务服务器', '172.20.174.12', '3', '002'), ('552605227929305088', '2019-03-05 21:35:53', '0', null, null, '使用中', '0', '0', null, '0', '2019-03-05 22:26:41', '3', '前端应用服务器', '192.168.1.123', '2', '002'), ('552605227929305089', '2019-03-05 21:35:53', '0', null, null, '使用中', '0', '0', null, '0', '2019-03-05 21:35:53', '4', '业务测试服务器', '192.168.2.124', '1', '002');
COMMIT;

-- ----------------------------
--  Table structure for `sms_fail`
-- ----------------------------
DROP TABLE IF EXISTS `sms_fail`;
CREATE TABLE `sms_fail` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `biz_id` varchar(255) DEFAULT NULL,
  `busness_id` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `out_id` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `sign_name` varchar(255) DEFAULT NULL,
  `template` varchar(255) DEFAULT NULL,
  `template_code` varchar(255) DEFAULT NULL,
  `validate_code` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sms_send`
-- ----------------------------
DROP TABLE IF EXISTS `sms_send`;
CREATE TABLE `sms_send` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `biz_id` varchar(255) DEFAULT NULL,
  `busness_id` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `out_id` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `sign_name` varchar(255) DEFAULT NULL,
  `template` varchar(255) DEFAULT NULL,
  `template_code` varchar(255) DEFAULT NULL,
  `validate_code` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sms_template`
-- ----------------------------
DROP TABLE IF EXISTS `sms_template`;
CREATE TABLE `sms_template` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `template_content` longtext,
  `template_desc` varchar(255) DEFAULT NULL,
  `template_id` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `tag`
-- ----------------------------
DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `tag_name` varchar(255) DEFAULT NULL,
  `tag_order` int(11) NOT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `apply_pass_account` varchar(255) DEFAULT NULL,
  `apply_time` datetime DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `head_img_url` varchar(255) DEFAULT NULL,
  `id_card` varchar(255) DEFAULT NULL,
  `integral` int(11) NOT NULL,
  `language` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `open_id` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `privileges` tinyblob,
  `province` varchar(255) DEFAULT NULL,
  `qq` varchar(255) DEFAULT NULL,
  `qr_scene` varchar(255) DEFAULT NULL,
  `qr_scene_str` varchar(255) DEFAULT NULL,
  `real_name` varchar(255) DEFAULT NULL,
  `real_name_tmpl` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `run_weight` varchar(255) DEFAULT NULL,
  `school_message` varchar(255) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `sex_desc` varchar(255) DEFAULT NULL,
  `subscribe` bit(1) DEFAULT NULL,
  `subscribe_scene` varchar(255) DEFAULT NULL,
  `subscribe_time` bigint(20) DEFAULT NULL,
  `tag_ids` tinyblob,
  `union_id` varchar(255) DEFAULT NULL,
  `user_remark` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `wechat_message`
-- ----------------------------
DROP TABLE IF EXISTS `wechat_message`;
CREATE TABLE `wechat_message` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `card_id` varchar(255) DEFAULT NULL,
  `consume_source` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `create_time` bigint(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `device_status` int(11) DEFAULT NULL,
  `device_type` varchar(255) DEFAULT NULL,
  `error_count` int(11) DEFAULT NULL,
  `event` varchar(255) DEFAULT NULL,
  `event_key` varchar(255) DEFAULT NULL,
  `expired_time` bigint(20) DEFAULT NULL,
  `fail_reason` varchar(255) DEFAULT NULL,
  `fail_time` bigint(20) DEFAULT NULL,
  `fee` varchar(255) DEFAULT NULL,
  `filter_count` int(11) DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `friend_user_name` varchar(255) DEFAULT NULL,
  `from_kf_account` varchar(255) DEFAULT NULL,
  `from_user` varchar(255) DEFAULT NULL,
  `hard_ware` tinyblob,
  `is_chat_room` varchar(255) DEFAULT NULL,
  `is_give_by_friend` int(11) DEFAULT NULL,
  `is_restore_member_card` varchar(255) DEFAULT NULL,
  `is_return_back` varchar(255) DEFAULT NULL,
  `kf_account` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `location_id` varchar(255) DEFAULT NULL,
  `location_name` varchar(255) DEFAULT NULL,
  `locationx` double DEFAULT NULL,
  `locationy` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `media_id` varchar(255) DEFAULT NULL,
  `menu_id` bigint(20) DEFAULT NULL,
  `modify_balance` varchar(255) DEFAULT NULL,
  `modify_bonus` varchar(255) DEFAULT NULL,
  `msg` varchar(255) DEFAULT NULL,
  `msg_id` bigint(20) DEFAULT NULL,
  `msg_type` varchar(255) DEFAULT NULL,
  `old_user_card_code` varchar(255) DEFAULT NULL,
  `op_type` int(11) DEFAULT NULL,
  `open_id` varchar(255) DEFAULT NULL,
  `original_fee` varchar(255) DEFAULT NULL,
  `outer_id` int(11) DEFAULT NULL,
  `outer_str` varchar(255) DEFAULT NULL,
  `pic_url` varchar(255) DEFAULT NULL,
  `poi_id` varchar(255) DEFAULT NULL,
  `precision_` double DEFAULT NULL,
  `recognition` varchar(255) DEFAULT NULL,
  `remark_amount` varchar(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `scale` double DEFAULT NULL,
  `sent_count` int(11) DEFAULT NULL,
  `staff_open_id` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `store_uniq_id` varchar(255) DEFAULT NULL,
  `thumb_media_id` varchar(255) DEFAULT NULL,
  `ticket` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `to_kf_account` varchar(255) DEFAULT NULL,
  `to_user` varchar(255) DEFAULT NULL,
  `total_count` int(11) DEFAULT NULL,
  `trans_id` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `user_card_code` varchar(255) DEFAULT NULL,
  `verify_code` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `wechat_template`
-- ----------------------------
DROP TABLE IF EXISTS `wechat_template`;
CREATE TABLE `wechat_template` (
  `id` varchar(36) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `application_id` varchar(36) DEFAULT NULL,
  `delete_manager` varchar(255) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `field_prop` varchar(255) DEFAULT NULL,
  `has_delete` int(11) DEFAULT NULL,
  `has_status` int(11) DEFAULT NULL,
  `school_code` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `error_message` longtext,
  `template_content` longtext,
  `template_desc` varchar(255) DEFAULT NULL,
  `template_id` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `master_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
