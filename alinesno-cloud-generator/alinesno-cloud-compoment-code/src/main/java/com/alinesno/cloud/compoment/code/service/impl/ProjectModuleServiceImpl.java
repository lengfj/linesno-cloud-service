package com.alinesno.cloud.compoment.code.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.alinesno.cloud.compoment.code.entity.ProjectModuleEntity;
import com.alinesno.cloud.compoment.code.repository.ProjectModuleRepository;
import com.alinesno.cloud.compoment.code.service.IProjectModuleService;

/**
 * <p> 项目选择模块 服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Service
public class ProjectModuleServiceImpl extends IBaseServiceImpl<ProjectModuleRepository, ProjectModuleEntity, String> implements IProjectModuleService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ProjectModuleServiceImpl.class);

}
