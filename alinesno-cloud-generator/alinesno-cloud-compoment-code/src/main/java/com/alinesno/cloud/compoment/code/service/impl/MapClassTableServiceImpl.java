package com.alinesno.cloud.compoment.code.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.alinesno.cloud.compoment.code.entity.MapClassTableEntity;
import com.alinesno.cloud.compoment.code.repository.MapClassTableRepository;
import com.alinesno.cloud.compoment.code.service.IMapClassTableService;

/**
 * <p> 类表映射信息 服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Service
public class MapClassTableServiceImpl extends IBaseServiceImpl<MapClassTableRepository, MapClassTableEntity, String> implements IMapClassTableService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(MapClassTableServiceImpl.class);

}
