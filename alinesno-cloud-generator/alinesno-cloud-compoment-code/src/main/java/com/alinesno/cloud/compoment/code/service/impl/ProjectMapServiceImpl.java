package com.alinesno.cloud.compoment.code.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.alinesno.cloud.compoment.code.entity.ProjectMapEntity;
import com.alinesno.cloud.compoment.code.repository.ProjectMapRepository;
import com.alinesno.cloud.compoment.code.service.IProjectMapService;

/**
 * <p> 项目数据表 服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Service
public class ProjectMapServiceImpl extends IBaseServiceImpl<ProjectMapRepository, ProjectMapEntity, String> implements IProjectMapService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ProjectMapServiceImpl.class);

}
