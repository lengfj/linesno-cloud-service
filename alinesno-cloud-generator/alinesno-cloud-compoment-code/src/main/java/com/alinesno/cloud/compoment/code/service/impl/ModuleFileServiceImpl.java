package com.alinesno.cloud.compoment.code.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.alinesno.cloud.compoment.code.entity.ModuleFileEntity;
import com.alinesno.cloud.compoment.code.repository.ModuleFileRepository;
import com.alinesno.cloud.compoment.code.service.IModuleFileService;

/**
 * <p> 模块文件信息 服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Service
public class ModuleFileServiceImpl extends IBaseServiceImpl<ModuleFileRepository, ModuleFileEntity, String> implements IModuleFileService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ModuleFileServiceImpl.class);

}
