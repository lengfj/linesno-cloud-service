<p>此为旧版本，新版本中台架构规划基线转移至组织[<a href="https://gitee.com/alinesno-cloud/alinesno-platform-press">alinesno-cloud</a>]维护</p>

<h2> 企业级技术中台</h2>

> 此基线设计更适合基础架构研发组，为企业提供技术中台架构,在技术中台上建设企业中台架构（中台业务）,因为是还在完善过程，一些内容图片引用其它平台，如有侵权，请告知

alinesno-cloud是基于`Dubbo SpringBoot`的`技术中台`，产出方向为企业基础架构和统一研发云平台，为企业提供技术中台，低代码开发平台
整体平台从
<br/>
<span style="font-weight:bold">`基础规范` - `组织结构` - `基础架构` - `业务开发` - `持续集成`- `自动化部署` - `自动化测试` - ` 生产运维监控` - `在线升级`</span>
<br/>
的全方位企业级技术中台开发解决方案，具有统一授权、认证后台管理系统，其中包含具备用户管理、资源权限管理、网关API
管理等多个模块，结合多个组件，为开发提供基础开发架构和支持，同时结合多种单点登陆方式(Cookies+SpringSession和Cas)，支持多业务系统并行开发。代码简洁，架构清晰，适合学习和直接项目(后期支持)中使用。
核心技术采用`Spring Boot 2.1.4`以及`Dubbo SpringBoot 1.7.1`相关核心组件。
<br/>

<p style="text-align:center">
<img src="https://img.shields.io/badge/Spring%20Cloud-Greenwich.RELEASE-blue.svg" alt="Coverage Status">
<img src="https://img.shields.io/badge/Spring%20Boot-2.1.4.RELEASE-blue.svg" alt="Downloads">

</p>

<!--
<p style="width:100%;float:left">
疑问讨论: QQ群 <a target="_blank" style="margin-top: 2px;position: absolute; margin-left: 10px;"  href="//shang.qq.com/wpa/qunwpa?idkey=bc44e8935e545891e1aa4ff2c2417cff0f21aef796c6ffc42bc79c462d8ee2ef"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="企业级系统架构师5群" title="企业级系统架构师5群"></a>
</p>
--> 

### 中台架构手册

- 中台架构手册：<a href="http://alinesno-document.beta.linesno.com/document-platform-design">访问</a>
- 中台部署手册：<a href="http://alinesno-document.beta.linesno.com/document-platform-operation">访问</a> 
- 应用开发手册：<a href="http://alinesno-document.beta.linesno.com/document-platform-service-technique">访问</a>

### 支持说明

> 为了可以更好的开源和支撑，以下为提供的技术支持，协助企业快速平台化和中台化

- 企业DevOps技术平台搭建和落地指导
- 企业自动化和持续集成体系支持
- 企业中台化和平台化架构规划和设计指导
- 企业过程技术答疑和技术指导

如需要技术手册原稿和技术支持，请关注公众号沟通：

<img src="./images/luoxiaodong.jpeg" width="150" >

### 第一个HelloWorld接入技术中台
通过一个hello world工程进行演示,用于开发人员项目指引工程，
开发并不需要了解整个平台架构及内部结构，此对开发透明化， 开发针对文档的示例及代码生成器即可生成
现成代码，进行二次开发，无需要从零配置，更专注业务实现。

1. 添加依赖
```xml
<!-- 引入依赖 -->
<dependencies>
    <dependency>
        <groupId>com.alinesno.cloud.common.web</groupId>
        <artifactId>alinesno-cloud-common-web-starter</artifactId>
    </dependency>
</dependencies>
<dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>com.alinesno.cloud</groupId>
            <artifactId>alinesno-cloud-dependencies</artifactId>
            <version>${最新版本号}</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
    </dependencies>
</dependencyManagement>
<repositories>
    <repository>
        <id>alinesno</id>
        <url>http://repository.lbxinhu.linesno.com</url>
        <snapshots>
            <enabled>true</enabled>
        </snapshots>
    </repository>
</repositories>
```

2. 编写启动类并运行
```java
@SpringBootApplication(exclude = DruidDataSourceAutoConfigure.class)
@EnableLogin
public class AlinesnoApplication {
    public static void main(String[] args) {
        SpringApplication.run(AlinesnoApplication.class, args);
    }
}
```

<!-- 3. 输入示例账号密码 `crm@gmail.com/admin`, 界面如下图： -->
<!-- <p style="text-align:center"><img src="/images/04_project_demo_02.png" width="80%" /></p> -->

<!-- 4. 整体工程如下图所求 -->
<!-- <p style="text-align:center"><img src="/images/04_project_demo_01.png" width="80%" /></p> -->

<!-- 以上即完成一个接入的hello工程，然后在此工程上面进行业务功能开发,如需配置，请到配置中心 -->
<!-- 配置菜单、代码、用户、权限、参数等。 -->


### 架构设计
此处设计并没有按网络或者相关论坛生搬硬照,会去掉一些操作如持续集成添加代码检测、使用cloud全家桶，
这些都可能会导致开发过程效率或者后期隐患，建议按企业实际团队情况考虑，此处针对的一般企业项目的基础架构设计。

### 技术中台架构设计原则

> 此技术中台架构设计有按的是通用型业务架构设计，而非指定于某种特定业务，以积累基础组件为前提,
> 在微服务架构做为载体，但是又有不一样的设计思路，此请注意。微服务架构设计着重对于服务划分，
> 而中台服务着重于中后台的沉淀和积累

- 按“重中台”+"轻应用"设计，业务应用逻辑思路放在前端应用，推荐是尽量减少或不拆分前端服务;
- 重中台的建设，在于前端应用共性部分的抽取和后期的沉淀，形成中台业务服务;
- 中台服务调用基础服务，或者其它同级服务，中台服务为服务的中层，用于业务共性(共享)抽取；
- 同一级服务之间可以互相调用，只能自下往下调用，平级调用，禁止自下往上调用，以避免服务混乱及维护混乱。
- 基础服务只为调用设计，位于服务的底层或者中间层，基础服务禁止调用中台服务；
- 服务单库设计,以减少迁移，服务之前影响等，每种服务目录按999个服务规划。

### 中台架构设计图

中台架构
<p style="text-align:center"><img src="/images/03_frame.png" width="80%" /></p>

中台服务能力案例示例

> 暂时从网上取下来类似的架构图

<p style="text-align:center"><img src="/images/04_business_cms.jpeg" width="80%" /></p>

### 组件层次说明
| 类型     | 目录名称 | 说明                                                                | 备注 |
|----------|----------|---------------------------------------------------------------------|------|
| 教程     | 示例服务 | 做示例工程，包含有所有服务调用示例                                  |      |
| 前端应用 | 门户服务 | 与中台服务同级，用于统一门户服务                                    |      |
| 前端应用 | 应用服务 | 前端应用或者手机应用                                                |      |
| 网关应用 | 网关服务 | 对外网关服务,与平台组件同级，但仅做为网关部分                       |      |
| 中台服务 | 中台服务 | 服务于前端应用，处理业务，可以服务之间互相调用，或者调用基础服务    |      |
| 基础服务 | 基础服务 | 公用基础组件，只能被调用或者调用公共或者组件包,不能主动调用其它服务 |      |
| 基础服务 | 公共服务 | 基础公共包,所有工程的基础，包括配置，页面，核心包等                 |      |
| 基础服务 | 组件服务 | 基础组件包，用于第三方等，组件包不能单独运行，只能被依赖            |      |
| 运维环境 | 监控服务 | 监控平台，用于运维平台，目前仅规划,有可能与平台服务合并一起         |      |
| 运维环境 | 平台服务 | 包括注册中心，配置中心等                                            | .    |

### 整体架构支撑
> 整体架构支撑是为了整体平台的流程，从管理、开发、测试、运维、生产几条线，实现整体平台的落地和管理

<p style="text-align:center"><img src="/images/01_design.png" width="80%" /></p>

### 技术中台门户
> 暂时引用其它平台logo，后面再设计

<table>
    <tr>
        <td style="vertical-align:top"><img src="/images/05_k8s.png"/></td>
        <td style="vertical-align:top"><img src="/images/06_zipkin.png"/></td>
    </tr>
    <tr>
        <td style="vertical-align:top"><img src="/images/01_portal.png"/></td>
        <td style="vertical-align:top"><img src="/images/03_portal.png"/></td>
    </tr>
    <tr>
        <td style="vertical-align:top"><img src="/images/02_portal.png"/></td>
        <td style="vertical-align:top"><img src="/images/01_document.jpg"/></td>
    </tr>
    <tr>
        <td style="vertical-align:top"><img src="/images/01_bootstrap.png"/></td>
        <td style="vertical-align:top"><img src="/images/02_cmdb.png"/></td>
    </tr>
    <tr>
        <td style="vertical-align:top"><img src="/images/01_generator.png"/></td>
        <td style="vertical-align:top"><img src="/images/01_soft.png"/></td>
    </tr>
    <tr>
        <td style="vertical-align:top"><img src="/images/02_learn.png"/></td>
        <td style="vertical-align:top"><img src="/images/01_learn.png"/></td>
    </tr>
    <tr>
        <td style="vertical-align:top"><img src="/images/01_cicd.png"/></td>
        <td style="vertical-align:top"><img src="/images/elk.jpg"/></td>
    </tr>
</table>


<!-- 研发门户 -->
<!-- <p style="text-align:center"><img src="/images/01_portal.png" width="80%" /></p> -->

<!-- 个人门户列表 -->
<!-- <p style="text-align:center"><img src="/images/03_portal.png" width="80%" /></p> -->

<!-- 门户管理后台 -->
<!-- <p style="text-align:center"><img src="/images/02_portal.png" width="80%" /></p> -->

<!-- ### 平台文档 -->
<!-- <p style="text-align:center"><img src="/images/01_document.jpg" width="80%" /></p> -->

<!-- ### 管理平台 -->
<!-- <p style="text-align:center"><img src="/images/01_bootstrap.png" width="80%" /></p> -->

<!-- ### 资产管理 -->
<!-- <p style="text-align:center"><img src="/images/02_cmdb.png" width="80%" /></p> -->

<!-- ### 代码生成 -->
<!-- <p style="text-align:center"><img src="/images/01_generator.png" width="80%" /></p> -->

<!-- ### 软件管理 -->
<!-- <p style="text-align:center"><img src="/images/01_soft.png" width="80%" /></p> -->

<!-- ### 学习平台 -->
<!-- <p style="text-align:center"><img src="/images/02_learn.png" width="80%" /></p> -->

<!-- ### 开发人员 -->
<!-- <p style="text-align:center"><img src="/images/01_learn.png" width="80%" /></p> -->

<!-- ### 持续集成和自动化 -->
<!-- <p style="text-align:center"><img src="/images/01_cicd.png" width="80%" /></p> -->

<!-- ## 相关基线 -->
<!-- | 序号 | 基线说明                     | 基线地址                          | 在线文档                 | 状态   | 备注 | -->
<!-- |------|------------------------------|-----------------------------------|--------------------------|--------|------| -->
<!-- | 1    | 平台环境搭建文档记录文档基线 | [alinesno-cloud-env][env]         | [在线文档][env_link]     | 集成中 |      | -->
<!-- | 2    | 研发人员服务列表代码基线     | [alinesno-cloud-service][service] | [在线文档][service_link] | 集成中 |      | -->
<!-- | 3    | 开发人员使用平台指引教程     | [alinesno-cloud-guide][guide]     | [在线文档][guide_link]   | 集成中 |      | -->
<!-- | 3    | 自动化测试测试研发基线       | [alinesno-cloud-test][guide]      | [在线文档][guide_link]   | 集成中 |      | -->
<!-- | 3    | 自动化运维研发基线           | [alinesno-cloud-operation][guide] | [在线文档][guide_link]   | 集成中 |      | -->

<!-- [env]: https://gitee.com/landonniao/linesno-cloud-env -->
<!-- [env_link]: http://gitbook.linesno.com/linesno-cloud-env/_book/ -->
<!-- [service]: https://gitee.com/landonniao/linesno-cloud-service -->
<!-- [service_link]: http://gitbook.linesno.com/linesno-cloud-service/_book/ -->
<!-- [guide]: https://gitee.com/landonniao/linesno-cloud-guide -->
<!-- [guide_link]: http://gitbook.linesno.com/linesno-cloud-guide/_book/ -->

## 开源鸣谢
> 参考了挺多优秀开源项目代码，平台只是一个整合，在此说明，如有缺漏，可提醒添加

- 前端工程参考开源项目 [rouyi][rouyi]
- 代码生成器和查询条件封装参考开源项目 [mybatis-plus][mybatis-plus]
- ...

[rouyi]: https://gitee.com/y_project/RuoYi
[mybatis-plus]: https://gitee.com/baomidou/mybatis-plus

## 相关文章
- [开发为什么要从零开始搭建属于自己的技术中台和中台架构](https://zhuanlan.zhihu.com/p/70488590)
- [我是怎么带几个学生从零开始做一个研发中台的](https://zhuanlan.zhihu.com/p/86393869)

## 参考资料
- 略

