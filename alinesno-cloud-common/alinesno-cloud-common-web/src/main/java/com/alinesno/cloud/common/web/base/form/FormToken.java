package com.alinesno.cloud.common.web.base.form;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 表单token<br/>
 * <input type="hidden" name="formToken" th:value="${session.form_token}" />
 * 
 * @author LuoAnDong
 * @since 2019年2月8日 上午10:40:26
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface FormToken {

	/**
	 * 生成 Token 标志
	 * 
	 * @return
	 */
	boolean save() default false;

	/**
	 * 移除 Token 值
	 * 
	 * @return
	 */
	boolean remove() default false;

}