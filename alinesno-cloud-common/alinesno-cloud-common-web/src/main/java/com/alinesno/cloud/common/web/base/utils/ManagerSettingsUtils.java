package com.alinesno.cloud.common.web.base.utils;

import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alinesno.cloud.base.boot.feign.dto.ManagerSettingsDto;
import com.alinesno.cloud.base.boot.feign.facade.ManagerSettingsFeigin;
import com.alinesno.cloud.common.core.context.ApplicationContextProvider;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

/**
 * 获取配置
 * @author Administrator
 *
 */
public class ManagerSettingsUtils {
	
	private static final Logger log = LoggerFactory.getLogger(ManagerSettingsUtils.class) ; 

	private static ManagerSettingsFeigin getSettings() {
		return ApplicationContextProvider.getBean(ManagerSettingsFeigin.class) ; 
	}

	/**
	 * 默认不打开验证码
	 * @return
	 */
	public static boolean isOpenCaptcha(){
		ManagerSettingsDto d = queryKey("sys.captcha.status" , null) ; 
		log.debug("d:{}" , d.toString());
		
		if("open".equals(d.getConfigValue())) {
			return true ; 
		}
	    return false ; 
	}
	
	public static ManagerSettingsDto queryKey(String key , String applicationId) {
		RestWrapper restWrapper = new RestWrapper() ; 
		restWrapper.eq("configKey", key) ; 
		if(StringUtils.isNotBlank(applicationId)) {
			restWrapper.eq("applicationId", applicationId) ; 
		}
	    Optional<ManagerSettingsDto> dto = getSettings().findOne(restWrapper) ; 
	    if(dto.isPresent() && dto.get() != null) {
	    	ManagerSettingsDto d = dto.get() ; 
	    	return d ; 
	    }
	    return null ; 
	}
	
}
