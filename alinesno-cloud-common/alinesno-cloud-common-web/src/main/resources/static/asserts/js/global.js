// 全局脚本
var _datatableId = "datatables" ; 
		
//////////////////////////////////////////////////// ajax start ///////////////////////////////////////////
$(function(){  
	var layerIndex ; 
    $.ajaxSetup({
        type: "POST",
    	contentType : "application/json;charset=utf-8",
    	timeout: 30000 ,
	    dataType:'json',
    	beforeSend:function(xhr){
        	console.log("ajax beforeSend = "+xhr) ;
        	layerIndex = layer.load(1, {shade: [0.8,'#000']});
    	},success:function(result){
        	console.log("ajax success = "+result) ;
        	if(result.status != 200){
        		layer.alert(result.desc) ; 
        	}
    	},
    	complete : function(XMLHttpRequest, textStatus) {
        	console.log("ajax complete = "+XMLHttpRequest + " , status = " + textStatus) ;
        	if(layerIndex != null){
        		layer.close(layerIndex) ; 
        	}
        },
        error: function(jqXHR, textStatus, errorThrown) {
        	console.log("ajax error = " + jqXHR + " , status = " + textStatus) ; 
        	layer.alert("服务响应失败,请联系管理员.") ; 
        }
   });
    
   // key search 
   enterSearch() ;  
});
//////////////////////////////////////////////////// ajax end ///////////////////////////////////////////

//////////////////////////////////////////////////// datatable start ///////////////////////////////////////////
/**
 * get params
 */
function paramsBuilder() {
    var param = {} ;
    $('#AlinesnoSortPanel input').each(function() {
        if ($(this).val() != '' && this.name != '') {
            param[this.name] = this.value;
        }

    });
    
    $('#AlinesnoSearchForm input').each(function() {
        if ($(this).val() != '' && this.name != '') {
            param[this.name] = this.value;
        }

    });
    
    $('#AlinesnoSearchForm .select').each(function() {
        if ($(this).val() != null &&  $(this).val() != '' && this.name != '') {
            param[this.name] = $(this).val() ;
        }
    });
    return param ;
}

//key event
function enterSearch(){
	$('#AlinesnoSearchForm input').each(function() {
		$(this).keyup(function(event){
		  if(event.keyCode ==13){
			  search(_datatableId) ; 
		  }
		});
	});
}

//搜索
function search(_datatables){
	console.log("search datatables:{}" , datatables)
	$('#'+_datatables).dataTable().fnDraw(false);
	// datatables.fnDraw(false);
}

// 初始化datatable
function initDatatable(
		_datatableId ,  
		_dataUrl , 
		_columns , 
		_toolbar , 
		_isSingle
	){
	initDatatablePro(_datatableId ,  _dataUrl , _columns , null , _toolbar , _isSingle) ;
}

// 初始化配置
function initDatatablePro(
		_datatableId ,  
		_dataUrl , 
		_columns , 
		_columnDefs ,
		_toolbar , 
		_isSingle
	){

	if(_columnDefs == null){
		_columnDefs = [] ; 
	}
	
	var dataTable = $('#'+_datatableId).DataTable({
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": _dataUrl , 
			"type":"POST" , 
			"headers" : {
				'Content-Type' : 'application/x-www-form-urlencoded' //multipart/form-data;boundary=--xxxxxxx   application/json
			},
			"data": function(d){
				var searchParams = paramsBuilder() ; 
				if(searchParams){
					$.extend(d , searchParams); // 添加扩展参数 
				}
			}
		} , 
		"processing" : true , //必须加上这个才能显示加载中的效果
		"serverSide" : true,
		"searching" : false, 
		"lengthMenu": [10, 20, 30, 50],
		"displayLength": 10,
		"scrollX": true ,
		"responsive": true ,
		"columns": _columns ,
		"columnDefs": _columnDefs ,
		"language": {
			"processing": "处理中...",
			"lengthMenu": "显示 _MENU_ 项结果",
			"zeroRecords": "没有找到相应的结果",
			"info": "第 _START_ 至 _END_ 行，共 _TOTAL_ 行",
			"infoEmpty": "第 0 至 0 项结果，共 0 项",
			"infoFiltered": "(由 _MAX_ 项结果过滤)",
			"infoPostFix": "",
			"search": "搜索:",
			"searchPlaceholder": "请输入要搜索内容...",
			"url": "",
			"thousands": "'",
			"emptyTable": "表中数据为空",
			"loadingRecords": "载入中...",
			"infoThousands": ",",
			"paginate": {
				"first": "首页",
				"previous": "上页",
				"next": "下页",
				"last": "末页"
			}
		},
		"dom": '<"toolbar">frtip'
	});

	if(_toolbar != null){
		var toolbarHtml = "" ; 
		for(var j = 0 ; j < _toolbar.length ; j ++){
			var item = _toolbar[j] ; 
			toolbarHtml += "<button type='button' onclick='"+ item.fMethod +"' class='btn btn-primary' style='float:right;margin-right:10px'>"+ item.fName +"</button>" ;
		}
		console.log("toolbar Html:" + toolbarHtml) ;
		$("div.toolbar").html('<b>'+ toolbarHtml +'</b>');
	}

	// 单选
	$('#'+_datatableId+' tbody').on( 'click', 'tr', function () {
		if ( $(this).hasClass('selected') ) {
			$(this).removeClass('selected');
		} else {
			dataTable.$('tr.selected').removeClass('selected');
			$(this).addClass('selected');
		}
	});
	
	return dataTable ; 
}

//////////////////////////////////////////////////// datatable end ///////////////////////////////////////////


//////////////////////////////////////////////////// curd start ///////////////////////////////////////////
function addItem(_datatableId , _url){
	layer.open({
	  type: 2,
	  title: '添加',
	  shadeClose: true,
	  shade: 0.8,
	  maxmin: true, //开启最大化最小化按钮
	  area: ['893px', '600px'],
	  content: _url , 
	  end: function () {
		  search(_datatableId) ; 
	  }
	});	
}

//保存操作
function save(_addForm , _url) {
	$("#"+_addForm).ajaxSubmit({
		url : _url , 
		type : "POST",
		headers : {
			'Content-Type' : 'application/x-www-form-urlencoded'
		},
		success : function(data) {
			if (data.code == 200) {
				layer.msg("操作成功!");
			} else {
				layer.alert(data.message);
			}
		}
	});
}

// 删除多元素
function deleteMultipleItem(_datatableId , _url){
	var selectRows = $('#'+_datatableId).DataTable().rows('.selected').data() ; 
	if(selectRows.length == 0){
		layer.alert("编辑请选择元素.")
		return ; 
	}
	var rowsId = new Array(); 
	for(var i = 0 ; i < selectRows.length ; i ++){
		rowsId.push(selectRows[i].id) ; 
	}
	
	var confirmWindow = layer.confirm('请确认是否删除？', {
	  btn: ['删除','取消'] //按钮
	}, function(){
		$.post(_url , {rowsId:rowsId} , function(response){
			console.log('response = ' + response) ; 
			layer.close(confirmWindow) ; 
			if(response.code == 200){
				search(_datatableId) ; 
			}else{
				layer.alert(response.message) ; 
			}
		})	
	});
}

// 修改单个元素
function modifyItem(_datatableId , _url){
	var selectRows = $('#'+_datatableId).DataTable().rows('.selected').data() ; 
	if(selectRows.length == 0){
		layer.alert("编辑请选择元素.")
		return ; 
	}
	if(selectRows.length > 1){
		layer.alert("编辑请选择一条元素.")
		return ; 
	}
	var row = selectRows[0] ; 
	layer.open({type: 2,title: '修改',shadeClose: true,shade: 0.8,maxmin: true, area: ['893px', '600px'],
		  content: _url + '?id='+row.id  , 
		  end: function () {
			  search(null) ; 
          }
	});	
}

//保存操作
function update(_updateForm , _url) {
	console.log('save');
	$("#"+_updateForm).ajaxSubmit({
		url : _url , 
		type : "POST",
		headers : {
			'Content-Type' : 'application/x-www-form-urlencoded' //multipart/form-data;boundary=--xxxxxxx   application/json
		},
		success : function(data) {
			if (data.code == 200) {
				layer.msg("操作成功!");
			} else {
				layer.alert(data.message);
			}
		}
	});
}

//////////////////////////////////////////////////// curd end ///////////////////////////////////////////


