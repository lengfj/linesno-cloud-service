<%--

    Licensed to Jasig under one or more contributor license
    agreements. See the NOTICE file distributed with this work
    for additional information regarding copyright ownership.
    Jasig licenses this file to you under the Apache License,
    Version 2.0 (the "License"); you may not use this file
    except in compliance with the License.  You may obtain a
    copy of the License at the following location:

      http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on an
    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied.  See the License for the
    specific language governing permissions and limitations
    under the License.

--%> 
<%@ page pageEncoding="UTF-8"%>
<jsp:directive.include file="includes/top.jsp" />
<body data-app-state="">
	<div id="root">
		<div>
			<div class="zJwEi">
				<div class="eaYrSC">
					<div class="jCprYf"></div>
					<div class="iMoAjM">
						<div class="vHoXo">
							<header class="dMTsYF">
								<jsp:directive.include file="includes/svg.jsp" />
							</header>
							<section role="main" class="ffnCIW">
								<div class="cMZWRp">
									<div>
										<div role="alert" class="bYGriA" style="box-shadow:none;">
											<h2><spring:message code="screen.success.header" /></h2>
										    <p><spring:message code="screen.success.success" /></p>
										    <p><spring:message code="screen.success.security" /></p>
										</div>
									</div>
								</div>
							</section>
							<jsp:directive.include file="includes/bottom.jsp" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>



